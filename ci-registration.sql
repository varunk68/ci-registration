-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2022 at 10:38 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci-registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lc_student_master`
--

CREATE TABLE `lc_student_master` (
  `LCSM_ID` int(11) NOT NULL,
  `LCSM_FIRSTNAME` varchar(100) NOT NULL,
  `LCSM_LASTNAME` varchar(100) NOT NULL,
  `LCSM_USERNAME` varchar(100) NOT NULL,
  `LCSM_PASSWORD` varchar(100) NOT NULL,
  `LCSM_ISDCODE` varchar(255) NOT NULL,
  `LCSM_MOBILENO` varchar(100) NOT NULL,
  `LCSM_STATUS` tinyint(4) NOT NULL,
  `LCSM_CREATED_BY` int(11) NOT NULL,
  `LCSM_CREATED_DATE` datetime NOT NULL,
  `LCSM_UPDATED_BY` int(11) NOT NULL,
  `LCSM_UPDATE_DATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lc_student_master`
--
ALTER TABLE `lc_student_master`
  ADD PRIMARY KEY (`LCSM_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lc_student_master`
--
ALTER TABLE `lc_student_master`
  MODIFY `LCSM_ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
