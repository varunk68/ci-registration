<?php
if (!defined('BASEPATH')) {
    exit('NO ACCESS');
}
class Master_model extends CI_Model
{


/* ---------------functions mostly use ---------------         */

    

    public function updatedata($field, $id, $arr, $table)
    {
        $this->db->where($field, $id);
        $res = $this->db->update($table, $arr);
        return $res;
    }
    public function Insertdata($table, $insertdata)
    {
        $this->db->insert($table, $insertdata);
        return $this->db->affected_rows();
    }

    public function InsertdatawithlastID($table, $insertdata)
    {
        $this->db->insert($table, $insertdata);
        return $this->db->insert_id();
    }

    public function select_edit_data($table, $field, $edit_id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field, $edit_id);
        $query = $this->db->get();
        return $query->result();
    }
	
    public function select_data($table, $field, $where)
    {
        $this->db->select($field);
        $this->db->from($table);
        if ($where != '') {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->result();
    }
    public function checklogin($table, $field, $where)
    {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where($where);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result();
            return $row;
        } else {
            return 'no';
        }
    }



	


}
