<!DOCTYPE html>
<html lang="en">
<head>
<title>Login / Registration</title>
<meta name="description" content="" />
<?php include 'header.php';?>
<link href="<?php echo ASSETS;?>jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo ASSETS;?>css/select2.min.css?v=1011440.0" rel="stylesheet" >
<link type="text/css" href="<?php echo ASSETS;?>dist/select2.optgroupSelect.css" rel="stylesheet" >
	<section id="content">
		<div class="container">	
			<div class="row"> 
				<div class="col-md-12">
					<div class="about-logo">
						<p>Home > Login / Register</p><hr>
					</div>  
				</div>
		   </div>
	   </div>
	</section>
	<section>
      <div class="container">
      <div class="row">
              <div class="container padL">
                <div class="alert alert-success alert-dismissable" style="<?php if($this->session->flashdata('success')) echo 'display:block'; else echo 'display:none';?>" >
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                </div>			
                <div class="alert alert-danger alert-dismissable" style="<?php if($this->session->flashdata('error')) echo 'display:block'; else echo 'display:none';?>">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Error!</strong> <?php echo $this->session->flashdata('error');?>
                </div>
              </div>		
          </div>


      	<div class="row">
          <div class="col-md-4 mb-40">
            <h4 class=" mt-0 pt-5"> Login</h4>
            <hr>
            <p></p>
            <form name="login-form" class="clearfix"  method="post" action="<?php echo base_url(); ?>sign-in" enctype="multipart/form-data">
            <div class="row mb0">
                <div class="form-group col-md-12">
                  <label for="email">Username/Email</label>
                  <input id="email" name="email" class="form-control" type="text">
                </div>
              </div>
              <div class="row mb0">
                <div class="form-group col-md-12">
                  <label for="password">Password</label>
                 <input id="password" name="password" class="form-control" type="password">
                </div>
              </div>
              <div class="row mb0">
              <div class="form-group pull-right ">
                <button class="btn  btn-block mt-15 btn-sm" type="submit">Login</button>
              </div>
              </div>
            </form>
          </div>
          <div class="col-md-7 col-md-offset-1">
            <form name="reg-form" class="register-form" method="post" action="<?php echo base_url(); ?>registration" enctype="multipart/form-data" >
              <div class="icon-box mb-0 p-0">
                <h4 class=" pt-10 mt-0 mb-30">Don't have an Account? Register Now.</h4>
              </div>
              <hr>
              <p class="text-gray"></p>
              <div class="row mb0">
                <div class="form-group col-md-6 col-xs-12 col-sm-12">
                  <label for="name">First Name</label>
<input type="text" id="name" name="name"  value="<?php echo  set_value('name'); ?>" class="form-control col-md-7 col-xs-12" required />
                </div>
                <div class="form-group col-md-6 col-xs-12 col-sm-12">
                  <label>Last Name</label>
                  <input name="lastname" class="form-control" type="text" value="<?php echo  set_value('lastname'); ?>" required />
                </div>
              </div>
              <div class="row mb0">
                <div class="form-group col-md-6 col-xs-12 col-sm-12">
                  <label for="email">Choose Email</label>
                  <input  name="email" class="form-control" type="email" required />
                </div>
			  	<div class="form-group col-md-2 col-xs-4 col-sm-4">
                  <label>ISD Code</label>
                  <input name="isdcode" class="form-control"  type="number"   value="<?php echo  set_value('isdcode'); ?>" required />
                </div>
			  	<div class="form-group col-md-4 col-xs-8 col-sm-8">
                  <label>Mobile no</label>
                  <input name="mobile" class="form-control" onkeyup="IsChkNumc(this);" onchange="IsChkNumc(this);" type="text" min="10" maxlength="15" value="<?php echo  set_value('mobile'); ?>" required />
                </div>
              </div>
              <div class="row mb0">
                <div class="form-group col-md-6  col-xs-12 col-sm-12">
                  <label for="password">Choose Password</label>
                  <input  name="password" class="form-control" type="password" required />
                </div>
                <div class="form-group col-md-6  col-xs-12 col-sm-12">
                  <label>Re-enter Password</label>
                  <input id="confpassword" name="confpassword"  class="form-control" type="password">
                </div>
              </div>
              <div class="form-group">
                <button class="btn  btn-lg btn-block mt-15" type="submit">Register</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
<?php include 'footer.php';?>
<script src="<?php echo ASSETS;?>js/jquery.js"></script>
<script src="<?php echo ASSETS;?>jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="<?php echo ASSETS;?>js/select2.js"></script>
<script src="<?php echo ASSETS;?>dist/select2.optgroupSelect.js"></script>

