<?php if(!$this->session->has_userdata('fid')){	redirect('login'); } ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="" />
<!-- Page Title -->
<title>My Account</title>
<?php include 'header.php';?>
<div class="main-content">

<?php if(isset($student)){
$studentfname = ''; $studentmname = ''; $studentlname = ''; $studentemail = ''; $studentimage = '';
foreach($student as $user) {
$studentfname = $user->LCSM_FIRSTNAME;
$studentlname = $user->LCSM_LASTNAME;
$studentemail = $user->LCSM_USERNAME;
}}
?>
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-sx-12 col-sm-3 col-md-3">
              <div class="doctor-thumb">
              </div>
              <div class="info p-20 bg-black-333">
                <h4 class="text-uppercase text-white"><?php echo ($studentfname.' '.$studentlname);?></h4>
                <ul class="list angle-double-right m-0">
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Email</strong><br><?php echo $studentemail; ?></li>
                </ul>
                <a class="btn btn-danger btn-flat mt-10 mb-sm-30" href="<?php echo base_url();?>logout">Logout</a>
              </div>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-9">
			
               <div>
<h2 style="color:red; text-align:center;">Welcome Dashboard</h2> <h3 style="color:blue; text-align:center;"><?php echo ($studentfname.' '.$studentlname);?></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  <!-- Footer -->
 <?php include 'footer.php';?>