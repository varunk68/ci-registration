<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="author" content="#"/>
<meta name="google" content="notranslate">
<link href="<?php echo ASSETS;?>css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo ASSETS;?>css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo ASSETS;?>css/fancybox/jquery.fancybox.css" rel="stylesheet"> 
<link href="<?php echo ASSETS;?>css/flexslider.css" rel="stylesheet" /> 
<link href="<?php echo ASSETS;?>css/style.css?v=150521.0" rel="stylesheet"/>
<link href="<?php echo ASSETS;?>css/style-main.css?v=150521.0" rel="stylesheet"/>
<link href="<?php echo ASSETS;?>css/utility-classes.css" rel="stylesheet"/>
<link href="<?php echo ASSETS;?>css/custom-bootstrap-margin-padding.css" rel="stylesheet"/>
<link href="<?php echo ASSETS;?>css/theme-skin-color-set-1.css" rel="stylesheet"/>
<link href="<?php echo ASSETS;?>css/animate.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo ASSETS;?>css/modaal.css">
<link type="text/css" href="<?php echo ASSETS;?>css/font-awesome.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic&display=swap" rel="stylesheet">
</head>
<body>
<div id="wrapper" class="home-page">
<div class="topbar">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-5 col-xs-12">
</div>
<div class="col-sm-12 col-md-7 col-xs-12"> <ul class="nav navbar-nav" style="float: right!important;">
<li><a class="navbar-nav2" href="<?php echo SITE_URL;?>">Home</a></li>
<?php  if($this->session->userdata('fid')){	$fname = $this->session->userdata('fname');	?>
<li> <a class="navbar-nav2" href="<?php echo base_url();?>my-account"> My Account (<?php echo $fname; ?>) </a>  </li>
<li><a class="navbar-nav2" href="<?php echo base_url();?>logout">Logout</a> </li>
<?php 	} else if($this->session->userdata('mfid')){ $mfname = $this->session->userdata('mfname'); ?>
<li> <a class="navbar-nav2" href="<?php echo base_url();?>mentor-account"> My Account (<?php echo $mfname; ?>) </a>  </li>
<li><a class="navbar-nav2" href="<?php echo base_url();?>mentor-logout">Logout</a> </li>	
<?php } else {?>
<li><a class="navbar-nav2" href="<?php echo SITE_URL;?>login">Login / Register</a></li>
<?php }?>
</ul>
</div>
</div>
</div>
</div>