<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('get_list')) {
    function get_list($tbl, $field, $where)
    {
        $CI =& get_instance();
        $CI->db->select($field);
        if ($where != '') {
            $CI->db->where($where);
        }
            
        $query=$CI->db->get($tbl);
        return $query->result();
    }
}

if (!function_exists('get_row')) {
    function get_row($tbl, $field, $where)
    {
        $CI =& get_instance();
        $CI->db->select($field);
        if ($where != '') {
            $CI->db->where($where);
        }
            
        $query=$CI->db->get($tbl);
        return $query->row();
    }
}

if (!function_exists('select_row')) {
    function select_row($field, $table, $condition)
    {
        $CI =& get_instance();
        $str="SELECT $field FROM $table WHERE $condition";
        $result = $CI->db->query($str);
        $row=$result->row();
        return($row);
    }
}





















if(!function_exists('get_student_name')){
	function get_student_name($web_id){
		$CI =& get_instance();
		if($web_id){
			$webdata = $CI->db->query("select LCSM_FIRSTNAME,LCSM_MIDDLENAME,LCSM_LASTNAME from lc_student_master where LCSM_ID = $web_id");
			$r = $webdata->row();
			if($r){
				return $r->LCSM_FIRSTNAME.' '.$r->LCSM_MIDDLENAME.' '.$r->LCSM_LASTNAME;
			}
		}
		
	}
}