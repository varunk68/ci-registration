<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
|  Facebook API Configuration
| -------------------------------------------------------------------
|
| To get an facebook app details you have to create a Facebook app
| at Facebook developers panel (https://developers.facebook.com)
|
|  facebook_app_id               string   Your Facebook App ID.
|  facebook_app_secret           string   Your Facebook App Secret.
|  facebook_login_type           string   Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string   URL to redirect back to after login. (do not include base URL)
|  facebook_logout_redirect_url  string   URL to redirect back to after logout. (do not include base URL)
|  facebook_permissions          array    Your required permissions.
|  facebook_graph_version        string   Specify Facebook Graph version. Eg v2.6
|  facebook_auth_on_load         boolean  Set to TRUE to check for valid access token on every page load.
*/
/* $config['facebook_app_id']              = '108786669827011';
$config['facebook_app_secret']          = '258ef061010d7776344eba7b7d0d5119';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'Checkout/facebook_login';
$config['facebook_logout_redirect_url'] = 'Checkout/facebook_logout';
$config['facebook_permissions']         = array('public_profile', 'publish_actions', 'email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE; */

$config['facebook_app_id']              = '329416635015615'; //'126962411000177';//'1654833434540922';//'108786669827011';
$config['facebook_app_secret']          = '0d34e4b5352a5ed255e74b9ffb34b3ca'; //'da0b3037b8de046e7a31e5c2b838fb9b';'82fe01ac319e4d59bf02eb215bfc4474';//'258ef061010d7776344eba7b7d0d5119';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'Checkout/facebook_login';
$config['facebook_logout_redirect_url'] = 'Checkout/facebook_logout';
$config['facebook_permissions']         = array('public_profile', 'publish_actions', 'email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE;