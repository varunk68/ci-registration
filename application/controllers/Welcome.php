<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends MY_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        
		$this->load->model('Master_model');	
	    $this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('email');
		date_default_timezone_set('Asia/Kolkata');
    }
	
    public function index()
    {  
        $this->load->view('home');
	}
	
	public function login()
    {  
        $this->load->view('login');
	}
	
	public function front_login()
{
	$this->form_validation->set_rules("email","Email","trim|required");
	$this->form_validation->set_rules("password","Password","trim|required");
	if($this->form_validation->run()==FALSE)
	{
		$this->session->set_flashdata('error', validation_errors());
		redirect('login');
	}
	else
	{
		$email = $this->input->post('email');
		$password = base64_encode($this->input->post('password'));	
		$pass = trim($password);
		$where = "LCSM_USERNAME='".$email."' and LCSM_PASSWORD = '".$pass."' and LCSM_STATUS = 1";
		$ans = $this->Master_model->checklogin("lc_student_master",'*',$where);	
		if($ans == 'no'){
			$this->session->set_flashdata('error',"Incorrect username and password combination or the account is not active");		
			redirect('login');
		}
		else
		{
			if(isset($ans) && !empty($ans))
			{
				foreach($ans as $obj)
				{
					$fid = $obj->LCSM_ID;
					$this->session->set_userdata('fid',$fid);
					$fname = $obj->LCSM_FIRSTNAME;
					$this->session->set_userdata('fname',$fname);	
					redirect('my-account');	
				}
			}
		}
	}
}

 public function register()	
 {		    
	 $this->form_validation->set_rules("name","Name","trim|required");	    
	 $this->form_validation->set_rules("lastname","Last Name","trim|required");  
	 $this->form_validation->set_rules("isdcode","isdcode","trim|required");	    
	 $this->form_validation->set_rules("mobile","Mobile","trim|required");
	 $this->form_validation->set_rules("email","email","trim|valid_email|is_unique[lc_student_master.LCSM_USERNAME]|required");	   
	 $this->form_validation->set_rules("password","Password","trim|required");		
	 $this->form_validation->set_rules("confpassword","Re-enter","trim|required|matches[password]");		       
	 $this->form_validation->set_error_delimiters('<p style="color:red;">', '</p>');				
	 if($this->form_validation->run()==true)		
	 { 			
		$name = $this->input->post('name');			
		$lastname = $this->input->post('lastname');			
		$isdcode = $this->input->post('isdcode');				
		$mobile = $this->input->post('mobile');						
		$email = $this->input->post('email');			
		$password = trim(base64_encode($this->input->post('password')));
		$arr=array(	'LCSM_FIRSTNAME'=>$name,
				   'LCSM_LASTNAME'=>$lastname,
		'LCSM_ISDCODE'=>$isdcode,						
		'LCSM_MOBILENO'=>$mobile,						
		'LCSM_USERNAME'=>$email,						
		'LCSM_PASSWORD'=>$password,	
		'LCSM_CREATED_DATE'=> date("Y-m-d H:i:s"),
		'LCSM_UPDATE_DATE'=> date("Y-m-d H:i:s"),
		'LCSM_STATUS' => 1
			);			
			$table="lc_student_master";			
			$insertdata=$this->Master_model->Insertdata($table,$arr); 
			if($insertdata == 1)			
			{
				$this->session->set_flashdata('success',"register done");				
				redirect('login');	
			}			
			else 
			{				
				$this->session->set_flashdata('error',"problem with register");				
				redirect('login');			
			}					
	}		
	else		
	{			
		$this->session->set_flashdata('error', validation_errors());			
		redirect('login');		
	}			
}


public function view_account()
{	
	$fid =  $this->session->userdata('fid'); 
	$editdata['student'] = $this->Master_model->select_edit_data("lc_student_master",'LCSM_ID',$fid);	
	$this->load->view('my-account',$editdata);	
}
	
public function signout()
{	
	$this->session->unset_userdata('fid');
	$this->session->unset_userdata('fname');
	redirect('login'); 
}	
}
